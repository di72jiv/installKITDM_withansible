This is a short guide to use *[Ansible](http://docs.ansible.com/)* and this
playbook to deploy *KIT Data Manager* on a remote host running *Ubuntu Xenial*.

## On Remote Host

1. Install remote machine with Ubuntu 16.04 LTS (tested distribution).
2. Install ansible dependencies on remote host.

```
  apt-get install openssh-server python
```

3. Enable ssh-access for root (or the `remote_user` set in `group_vars/all`) on the remote host.

## On Local Machine

We assume you have `ssh` installed and craeted a key. If not, refer to
[a basic tutorial](https://help.ubuntu.com/community/SSH/OpenSSH/Keys). 

1. Install ansible

```
  apt-get install ansible 
```

2. Clone KITDM ansible repo:

```
  git clone https://gitlab.lrz.de/GeRDI/installKITDM_withansible.git installKITDM_withansible
  cd installKITDM_withansible
```

3. Add remote host(s) to the inventory in the `hosts` file: 

```
  [kitdm]
  <your-kitdm-machine>
```

4. Run the playbook on your local machine:

```
  ansible-playbook playbook.yml -i hosts
```

Ansible will run all tasks in the playbook. After completion continue KITDM
configuration at the following URL: `http://<your-kitdm-machin>:8080/KITDM/`.
Please note the uppercase URI part (KITDM).

For more information please refer to the
[manual](http://datamanager.kit.edu/dama/manual/#ChapterInstallation)
